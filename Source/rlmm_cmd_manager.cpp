//
//  rlmm::rlmm_cmd_manager
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//  ----------------------------------------------------------------------
//  This file is one implementation of how to build a command manager for
//  processing interpretive console text.  The cmd_manager class manages
//  the overall process of adding commands and running the process loop.
//  The cmd_completer class handles auto completion of command strings
//  as the user types.
//
//  Parts of this software were copied from examples by Christian Holm
//  Christensen and were then modified by Brent Yates.
//
#include <cstdlib>
#include <iostream>
#include <boost/ptr_container/ptr_list.hpp>
#include <optionmm_cmd_line.hpp>
#include <rlmm_readline.hpp>
#include <rlmm_buffer.hpp>
#include <rlmm_completion.hpp>
#include <rlmm_terminal.hpp>
#include <rlmm_history.hpp>

#include <rlmm_console_base_cmd.hpp>
#include <rlmm_cmd_manager.hpp>

using namespace std;

namespace rlmm
{

cmd_manager::cmd_manager(const string& prompt, const string& historyFilePath) :
		m_Prompt(prompt), m_HistoryFilePath(historyFilePath), m_Completer( *this), m_History()
	{
	name("cmd_manager");
	m_History.read(m_HistoryFilePath);
	m_History.activate();
	m_Done = false;
	m_WidestCommandWidth = 0;
	}

// TODO: Fix command width alignment issues
bool cmd_manager::help(const string& arg)
	{
	int printed = 0;
	for (cmd_list_itor_t i = m_Commands.begin(); i != m_Commands.end(); i++)
		{
		if (arg.empty())
			{
			cout << ( *i).Name << string(m_WidestCommandWidth-( *i).Name.length()+4,' ');
			cout << ( *i).ShortHelpText << endl;
			printed++;
			}
		else
			{
			if (( *i).Name.compare(CMP_ARG(arg))==0)
				{
				( *i).PrintLongHelp(m_WidestCommandWidth+4);
				printed++;
				}
			}
		}
	if (printed)
		{
		return (true);
		}
	cout << "No commands match `" << arg << "'. Possibilities are:" << endl;
	for (cmd_list_itor_t i = m_Commands.begin(); i != m_Commands.end(); i++)
		{
		if (printed == 6)
			{
			cout << endl;
			printed = 0;
			}
		cout << ( *i).Name << "\t";
		printed++;
		}
	if (printed)
		{
		cout << endl;
		}
	return (true);
	}

string cmd_manager::make_prompt()
	{
	return(m_Prompt);
	}

int cmd_manager::run()
	{
	string line;
	while ( !m_Done)
		{
		if ( !read(make_prompt(), line)) break;

		if (line.empty()) continue;

		string expansion;
		int rt = m_History.expand(line, expansion);
		if (rt)
			{
			line = expansion;
			}

		string InitialCmdText = m_History.arguments(0, 0, line);
		m_Completer.set_start(true);
		list<string> MatchList = m_Completer.matches(InitialCmdText);
		if (MatchList.size() > 1)
			{
			cerr << "Ambiguous command " << InitialCmdText << std::endl;
			m_History.add(line);
			continue;
			}
		op_command* Command_ptr = 0;
		if (MatchList.size() == 1)
			{
			string FullCmdText = *(MatchList.begin());
			for (cmd_list_itor_t i = m_Commands.begin(); i != m_Commands.end(); i++)
				{
				if (FullCmdText == ( *i).Name)
					{
					Command_ptr = &( *i);
					break;
					}
				}
			}
		if ( !Command_ptr)
			{
			cerr << "No such command: `" << InitialCmdText << "'" << endl;
			m_History.add(line);
			continue;
			}

		if (Command_ptr->TrackInHistory)
			{
			m_History.add(line);
			}

		// Assemble arguments into a vector.
		arg_vector_t ArgVector;
		string arg;
		int n = 1;
		do
			{
			arg = m_History.arguments(n, n, line);
			if (arg.empty()) break;
			ArgVector.push_back(arg);
			n++;
			}
		while (true);
		Command_ptr->operator()(ArgVector);
		}
	return (0);
	}

std::list<std::string> cmd_completer::alternate_completer(const string& what, int start, int stop)
	{
	m_Start = true;
	std::list<std::string> l;
	if (start == 0)
		{
		l = matches(what);
		return (l);
		}
	m_Start = false;
	return (l);
	}

void cmd_completer::possibilities(const std::string& what)
	{
	m_CurrentCmd = m_Manager.commands().begin();
	}

string cmd_completer::complete(const std::string& what)
	{
	if ( !m_Start) return (filename_complete(what));

	string ret;
	while (m_CurrentCmd != m_Manager.commands().end())
		{
		if ( !( *m_CurrentCmd).Name.compare(CMP_ARG(what)))
			{
			ret = ( *m_CurrentCmd).Name;
			break;
			}
		m_CurrentCmd++;
		}
	if (m_CurrentCmd != m_Manager.commands().end())
		{
		m_CurrentCmd++;
		}
	return (ret);
	}

} // end namespace
