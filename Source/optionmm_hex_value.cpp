//
//  optionmm_hex_value
//  Copyright (C) 2013 Brent Yates <brent.yates@gmail.com>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//  ----------------------------------------------------------------------
//  This file creates a class which knows how to convert values from
//  hex strings to integers and back.  It was created to support the
//  optionmm library but may find other uses.
//
#include <iostream>
#include <sstream>
#include <optionmm_hex_value.hpp>

std::istream& operator>>(std::istream& is, HexValue_t& theHexOpt)
	{
	std::string str_arg;
	std::stringstream hex_arg;
	is >> str_arg;
	theHexOpt.set_value(str_arg);
	return(is);
	}

std::ostream& operator<<(std::ostream& os,const HexValue_t& theHexOpt)
	{
	os << theHexOpt.as_int();
	return(os);
	}

int operator!(const HexValue_t& theHexOpt)
	{
	return(!theHexOpt.as_int());
	}

