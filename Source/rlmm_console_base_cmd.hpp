//
//  rlmm::rlmm_console_base_cmd
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//  ----------------------------------------------------------------------
//  This file is one implementation of a base command class for use with
//  the rlmm ReadLine library system.
//
//  Parts of this software were copied from examples by Christian Holm
//  Christensen and were then modified by Brent Yates.
//
#pragma once

#include <vector>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <boost/ptr_container/ptr_list.hpp>

namespace rlmm
{

typedef std::vector<std::string> arg_vector_t;
typedef std::vector<std::string>::iterator arg_vector_itor_t;
typedef std::vector<std::string>::const_iterator arg_vector_const_itor_t;

/**
 * Abstract base class for all commands.
 */
class op_command
	{
	public:
	const std::string Name;
	const std::string ShortHelpText;
	bool TrackInHistory;
	op_command(const std::string& name = std::string(),
	           const std::string& shortHelp = std::string()) :
			Name(name), ShortHelpText(shortHelp), TrackInHistory(true)
		{
		}
	virtual ~op_command()
		{
		}
	// What to do
	virtual bool operator()(const arg_vector_t& arg_vector) = 0;
	virtual bool valid_arg(const std::string& arg)
		{
		if (arg.empty())
			{
			return (false);
			}
		return (true);
		}
	virtual void PrintLongHelp(const int indent = 0, std::ostream& os = std::cout)
		{
		os << Name << std::string(std::max((int)(indent-Name.length()),2),' ');
		os << ShortHelpText << std::endl;
		}
	};

typedef boost::ptr_list<op_command> cmd_list_t;
typedef boost::ptr_list<op_command>::iterator cmd_list_itor_t;
typedef boost::ptr_list<op_command>::const_iterator cmd_list_const_itor_t;

class cmd_manager;

} // end namespace
