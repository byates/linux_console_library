//
//  rlmm::rlmm_cmd_manager
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//  ----------------------------------------------------------------------
//  This file is one implementation of how to build a command manager for
//  processing interpretive console text.  The cmd_manager class manages
//  the overall process of adding commands and running the process loop.
//  The cmd_completer class handles auto completion of command strings
//  as the user types.
//
//  Parts of this software were copied from examples by Christian Holm
//  Christensen and were then modified by Brent Yates.
//
#pragma once

#include <cstdlib>
#include <iostream>
#include <boost/ptr_container/ptr_list.hpp>

#include <rlmm_readline.hpp>
#include <rlmm_completion.hpp>
#include <rlmm_history.hpp>

#include <rlmm_console_base_cmd.hpp>

namespace rlmm
{

#define CMP_ARG(x) size_t(0), x.size(), x

// Forward declarations
class cmd_manager;

/**
 *  Custom class for keyword completion.
 */
class cmd_completer: public rlmm::completion
	{
	private:
	cmd_manager& m_Manager;
	cmd_list_const_itor_t m_CurrentCmd;
	bool m_Start;

	public:
	cmd_completer(cmd_manager& fm) :
			m_Manager(fm)
		{
		this->activate();
		m_Start = true;
		}
	void set_start(const bool value)
		{
		m_Start = value;
		}

	/**
	 * Construct list of possibilities.
	 * This just reinitializes the iterator to point at the beginning of the command list.
	 * @param what What to do completion for.
	 */
	void possibilities(const std::string& what);
	/**
	 * Return next completion.
	 * If we're doing command completion (m_Start is true), then this loops until it finds a
	 * (possible) partial match in the command list and then return the name of the command.
	 * Otherwise if calls the rlmm::completion::filename_complete() member function to do
	 * file name completion.
	 *
	 * @param what what to match.
	 * @return name of command partial match found, or empty string.
	 */
	std::string complete(const std::string& what);
	/**
	 * The first thing to try.
	 * If we at the beginning of a line (@p start is 0) then we do command completion via normal
	 * completion functions.  Otherwise we do filename completion.
	 * @param what What to complete.
	 * @param start position in line of @p what.
	 * @param stop end position in line of @p what.
	 * @return a list of possible completions.
	 */
	std::list<std::string> alternate_completer(const std::string& what, int start, int stop);
	};

class cmd_manager: public rlmm::readline
	{
	private:
	std::string m_Prompt;
	std::string m_HistoryFilePath;
	cmd_completer m_Completer;
	rlmm::history m_History;
	cmd_list_t m_Commands;
	int m_WidestCommandWidth;
	bool m_Done;

	public:
	cmd_manager(const std::string& prompt, const std::string& historyFilePath);
	virtual ~cmd_manager()
		{
		m_History.write(m_HistoryFilePath);
		}
	int run();
	bool help(const std::string& arg);
	bool quit()
		{
		m_Done = true;
		return (m_Done);
		}
	void add_command(op_command* pCommand)
		{
		int CommandWidth = pCommand->Name.length();
		if (CommandWidth > m_WidestCommandWidth) { m_WidestCommandWidth = CommandWidth; }
		m_Commands.push_back(pCommand);
		}
	const cmd_list_t& commands() const
		{
		return (m_Commands);
		}
	std::string make_prompt();
	rlmm::history& History() {return(m_History);};
	};

} // end namespace
