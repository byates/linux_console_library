//
//  optionmm_hex_value
//  Copyright (C) 2013 Brent Yates <brent.yates@gmail.com>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//  ----------------------------------------------------------------------
//  This file creates a class which knows how to convert values from
//  hex strings to integers and back.  It was created to support the
//  optionmm library but may find other uses.
//
#ifndef OPTIONMM_HEX_VALUE_HPP
#define OPTIONMM_HEX_VALUE_HPP

#include <iostream>
#include <sstream>

/**
 * This class implements simple HEX value storage.  The value can be created from an integer
 * or a string (that is in valid hex format).
 */
class HexValue_t
	{
	public:
	HexValue_t() : m_Value(0) {}
	HexValue_t(unsigned int value) : m_Value(value) {}
	HexValue_t(const std::string str_value)
		{
		set_value(str_value);
		}
	void set_value(const std::string str_value)
		{
		std::stringstream hex_arg;
		hex_arg << std::hex << str_value;
		hex_arg >> m_Value;
		}
	void set_value(const unsigned int value)
		{
		m_Value = value;
		}
	unsigned int  as_int() const
		{
		return(m_Value);
		}
	std::string as_str() const
		{
		std::stringstream hex_arg;
		hex_arg << std::hex << m_Value;
		return(hex_arg.str());
		}
	private:
	unsigned int m_Value;
	};

std::istream& operator>>(std::istream& is, HexValue_t& theHexOpt);

std::ostream& operator<<(std::ostream& os,const HexValue_t& theHexOpt);

int operator!(const HexValue_t& theHexOpt);

#endif
