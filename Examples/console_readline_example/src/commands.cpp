//
//  commands
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//  ----------------------------------------------------------------------
//  This file is an example of how to create command classes for use by
//  the rlmm readline system.
//
//  Parts of this software were copied from examples by Christian Holm
//  Christensen and were then modified by Brent Yates.
//
#include <stdint.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <boost/format.hpp>
#include <boost/io/ios_state.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

#include <ConsoleLibrary.hpp>
#include <global_context.hpp>

using namespace std;
using namespace rlmm;

using boost::lexical_cast;
using boost::bad_lexical_cast;
using boost::format;
using boost::io::group;

/**
 * Another base class, this one with our global_context member
 */
struct op_command_with_context: public op_command
	{
	global_context_t& gc;
	op_command_with_context(global_context_t& global_context,
	                        const std::string& name = std::string(),
							const std::string& help = std::string()) :
			op_command(name, help), gc(global_context)
		{
		}
	};

/**
 * List directory command.
 */
struct cmd_ls: public op_command_with_context
	{
		cmd_ls(global_context_t& gc) :
				op_command_with_context(gc, "ls", "list files in directory")
			{
			}
		bool operator()(const arg_vector_t& arg_vector)
			{
			string ShellCmd("ls -FClg ");
			arg_vector_const_itor_t arg_item;
			for (arg_item = arg_vector.begin(); arg_item != arg_vector.end(); arg_item++)
				{
				if (arg_item != arg_vector.begin())
					{
					ShellCmd += " ";
					}
				ShellCmd += *arg_item;
				}

			int rt = std::system(ShellCmd.c_str());
			if (rt)
				{
				return (false);
				}
			return (true);
			}
	};

/**
 * Show contents of a file.
 */
struct cmd_more: public op_command_with_context
	{
		cmd_more(global_context_t& gc) :
				op_command_with_context(gc, "more", "View contents of  a file")
			{
			}
		bool operator()(const arg_vector_t& arg_vector)
			{
			string ShellCmd("more ");
			arg_vector_const_itor_t arg_item;
			for (arg_item = arg_vector.begin(); arg_item != arg_vector.end(); arg_item++)
				{
				if (arg_item != arg_vector.begin())
					{
					ShellCmd += " ";
					}
				ShellCmd += *arg_item;
				}
			int rt = std::system(ShellCmd.c_str());
			if (rt)
				{
				return (false);
				}
			return (true);
			}
	};

/**
 * Quit the manager
 */
struct cmd_quit: public op_command_with_context
	{
		cmd_quit(global_context_t& gc) :
				op_command_with_context(gc, "quit", "Quit the manager")
			{
			TrackInHistory = false;
			}
		bool operator()(const arg_vector_t& arg_vector)
			{
			return (gc.cmd_mgr.quit());
			}
	};

/**
 * Help command
 */
struct cmd_help: public op_command_with_context
	{
		cmd_help(global_context_t& gc) :
				op_command_with_context(gc, "help", "Show help. Usage help [cmd]")
			{
			}
		bool operator()(const arg_vector_t& arg_vector)
			{
			string arg = "";
			if (arg_vector.size() > 0)
				{
				arg = arg_vector[0];
				}
			return (gc.cmd_mgr.help(arg));
			}
	};

/**
 * History command
 */
struct cmd_history: public op_command_with_context
	{
		cmd_history(global_context_t& gc) :
				op_command_with_context(gc, "history", "Displays the command history.")
			{
			TrackInHistory = false;
			}
		bool operator()(const arg_vector_t& arg_vector)
			{
			boost::io::ios_all_saver guard(cout);
			rlmm::history& h = rlmm::history::active();
			const rlmm::history::entry_list& history_list = h.list();
			size_t entry_index = history_base;
			rlmm::history::entry_list::const_iterator entry;
			for (entry = history_list.begin(); entry != history_list.end(); ++entry, ++entry_index)
				{
				cout << setw(5) << entry_index << "  " << ( *entry).line() << endl;
				}
			return (true);
			}
	};

/**
 * Register read command
 *
 * Usage: reg_read <offset> [count]
 */
struct cmd_reg_read: public op_command_with_context
	{
		cmd_reg_read(global_context_t& gc) :
				op_command_with_context(gc, "reg_read", "Read register command.")
			{
			}
		void PrintLongHelp(const int indent = 0, std::ostream& os = std::cout)
			{
			string Indent(indent, ' ');
			os << Name << string(max(indent - (int)Name.length(), 1), ' ') << ShortHelpText << endl;
			os << Indent << "Usage: reg_read <hex offset> [count]" << endl;
			}
		bool operator()(const arg_vector_t& arg_vector)
			{
			register_value_array_t ValuesList;
			uint32_t Count;
			HexValue_t Offset(0);
			boost::io::ios_all_saver guard(cout);

			if (arg_vector.size() < 1)
				{
				cout << "ERROR: Not enough arguments." << endl;
				this->PrintLongHelp();
				return (false);
				}
			try
				{
				Offset.set_value(arg_vector[0]);
				if (arg_vector.size() >= 2)
					{
					Count = lexical_cast<uint32_t>(arg_vector[1]);
					}
				else
					{
					Count = 1;
					}
				// Use the gc class to get access to a device function which 'reads' multiple
				// registers.
				// EX: gc.ExampleDevice->register_read_multi(Offset.as_int(), Count, ValuesList);
				}
			catch (bad_lexical_cast &)
				{
				cout << "ERROR: bad arguments to reg_read.  See help." << endl;
				return (false);
				}
			catch (invalid_argument &e)
				{
				cout << "ERROR:" << e.what() << endl;
				return (false);
				}

			// display data
			register_value_array_itor_t RegItem;
			int Index = 0;
			for (RegItem = ValuesList.begin(); RegItem != ValuesList.end(); RegItem++)
				{
				if ((Index % 4) == 0)
					{
					if (Index)
						{
						cout << endl;
						}
					cout << setw(4) << setfill('0') << hex << Offset.as_int() + Index << ": ";
					}
				cout << setw(8) << setfill('0') << hex << ( *RegItem) << " ";
				Index++;
				}
			cout << endl;

			return (true);
			}
	};

/**
 * Register write command
 *
 * Usage: reg_write <offset> <values>+
 */
struct cmd_reg_write: public op_command_with_context
	{
		cmd_reg_write(global_context_t& gc) :
				op_command_with_context(gc, "reg_write", "Register write command.")
			{
			}
		void PrintLongHelp(const int indent = 0, std::ostream& os = std::cout)
			{
			string Indent(indent, ' ');
			os << Name << string(max(indent - (int)Name.length(), 1), ' ') << ShortHelpText << endl;
			os << Indent << "Usage: reg_write <hex offset> <hex values>+" << endl;
			}
		bool operator()(const arg_vector_t& arg_vector)
			{
			register_value_array_t ValuesList;
			HexValue_t Offset(0);
			HexValue_t HexArg(0);

			if (arg_vector.size() < 2)
				{
				cout << "ERROR: Not enough arguments." << endl;
				this->PrintLongHelp();
				return (false);
				}
			try
				{
				Offset.set_value(arg_vector[0]);
				arg_vector_const_itor_t Arg_itor;
				for (Arg_itor = arg_vector.begin() + 1; Arg_itor < arg_vector.end(); Arg_itor++)
					{
					HexArg.set_value( *Arg_itor);
					ValuesList.push_back(HexArg.as_int());
					}
				// Use the gc class to get access to a device function which 'writes' multiple
				// registers.
				// EX: gc.ExampleDevice->register_write_multi(Offset.as_int(), ValuesList);
				}
			catch (invalid_argument &e)
				{
				cout << "ERROR:" << e.what() << endl;
				return (false);
				}

			return (true);
			}
	};


/**
 * Register the commands with the command manager.
 *
 * @param theCmdManager
 */
void RegisterCommands(global_context_t& global_context)
	{
	global_context.cmd_mgr.add_command(new cmd_history(global_context));
	global_context.cmd_mgr.add_command(new cmd_help(global_context));
	global_context.cmd_mgr.add_command(new cmd_quit(global_context));
	global_context.cmd_mgr.add_command(new cmd_ls(global_context));
	global_context.cmd_mgr.add_command(new cmd_more(global_context));
	global_context.cmd_mgr.add_command(new cmd_reg_read(global_context));
	global_context.cmd_mgr.add_command(new cmd_reg_write(global_context));
	}

