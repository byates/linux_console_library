//
//  main.cpp
//  Copyright (C) 2013 Brent Yates <brent.yates@gmail.com>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//  ----------------------------------------------------------------------
//  This file is the main entry point for an example application that
//  shows how to use the rlmm readline library functions and classes.
//  It also shows how to use the optionmm libaray for processing application
//  arguments.
//
#include <stdint.h>
#include <cstdlib>
#include <iostream>
#include <ConsoleLibrary.hpp>

#include <global_context.hpp>

using namespace std;

const string HIST_FILE = ".cmd_hist";

void RegisterCommands(global_context_t& global_context);

int main(int argc, char** argv)
	{
	optionmm::basic_command_line<> cl("Example Console Application",
	                                  "1.0",
									  "Copyright (c) 2013 Brent Yates",
									  "Usage: console <options>",
									  argc, argv);

	optionmm::int_option i_opt('i', "int", "Integer option", 0);
	optionmm::basic_option<HexValue_t> h_opt('x', "hex", "Hex option", HexValue_t());

	cl.add(i_opt);
	cl.add(h_opt);

	// This method processes the command line arguments and looks for the matches defined above.
	// Any arguments NOT found are left in argc/argv.  The remaining args in argc/argv are
	// positional arguments and will show up in cl.positional_args.
	if ( !cl.process())
		{
		return (1);
		}

	// Prints help information if the help option was found.
	if (cl.help())
		{
		return (0);
		}

	// Prints version information.  The default is to print only if the version option was
	// found.  Here we override that behavior to print the information always.
	cl.version(cout, true);

	rlmm::cmd_manager cmd_mgr(">", HIST_FILE);

	// Global context is used to pass data to all the commands.  This class should be
	// initialized with any other classes or structures needed, such as a class which
	// provides access to a driver or device.
	global_context_t global_context(cmd_mgr, NULL);

	// register commands
	RegisterCommands(global_context);

	cmd_mgr.run();

	return (0);
	}
