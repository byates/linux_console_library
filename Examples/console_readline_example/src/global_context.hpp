//
//  global_context
//  Copyright (C) 2013 Brent Yates <brent.yates@gmail.com>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//  ----------------------------------------------------------------------
//  This file is one implementation of how to pass a global context
//  structure to all instances of the command classes.
//
#ifndef GLOBAL_CONTEXT_HPP_
#define GLOBAL_CONTEXT_HPP_

#include <ConsoleLibrary.hpp>

/**
 * Example of passing a structure with global context information to all the commands.
 */
struct global_context_t
	{
	rlmm::cmd_manager& cmd_mgr;

	void*  ExampleDeviceClass;

	global_context_t(rlmm::cmd_manager& cmd_mgr, void* exampleClass) :
		cmd_mgr(cmd_mgr), ExampleDeviceClass(exampleClass)
		{
		}
	};


// Example typedefs for a device register set.

typedef std::vector<uint32_t> register_value_array_t;
typedef std::vector<uint32_t>::const_iterator register_value_array_itor_t;

#endif /* GLOBAL_CONTEXT_HPP_ */
