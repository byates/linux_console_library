#include <cstdlib>
#include <iostream>
#include <ConsoleLibrary.hpp>

using namespace std;

int main(int argc, char** argv)
	{
	optionmm::basic_command_line<> cl(
			"Example Console Application",
			"1.0",
			"Copyright (c) 2013 Brent Yates",
			"Usage: console <options>",
			argc, argv);

	optionmm::int_option i_opt('i', "int", "Integer option", 0);
	optionmm::basic_option<HexValue_t> h_opt('x', "hex", "Hex option", HexValue_t());

	cl.add(i_opt);
	cl.add(h_opt);

	// This method processes the command line arguments and looks for the matches defined above.
	// Any arguments NOT found are left in argc/argv.  The remaining args in argc/argv are
	// positional arguments and will show up in cl.positional_args.
	if ( !cl.process())
		{
		return (1);
		}

	// Prints help information if the help option was found.
	if (cl.help())
		{
		return (0);
		}

	// Prints version information.  The default is to print only if the version option was
	// found.  Here we override that behavior to print the information always.
	cl.version(cout, true);

	for (int i = 0; i < h_opt.size(); i++)
		{
		// .value(n) returns the option 'type' object.  In this case that is HexValue_t.
		cout << h_opt.value(i).as_str() << endl;
		}

	for (unsigned int i = 0; i < cl.positional_args.size(); i++)
		{
		cout << cl.positional_args[i] << endl;
		}
	return (0);
	}
