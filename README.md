This package implements a full GNU Readline interface in C++. The API is quite simple and makes heavy use of templated container, standard classes and so on. The idea is that a C++ developer should feel resonabily comfortable with the library API.

NOTE: The RLMM and OPTIONMM libraries were copied from Christian Holm Christensen.  To get the original versions please see the main web site is at http://cholm.home.cern.ch/cholm/misc/#rlmm.

Mr. Christensen didn't publish the code to a repository site (such as SourceForge, BitBucket, GitHub, etc.) and as far as I can tell the only way to get the source is to download a GZ file.

My purpose in modifiying these files was to create a system for building an interactive console application where it is easy to add user commands.  I have made the following modifications to the optionmm and rlmm libraries.

* renamed the files to add a prefix for the library name.  This was done to limit file name collisions.
* Flattened the directory structure and merged optionmm and rlmm files into one directory.
* Took original example applications for rlmm and extracted the parts needed for an interactive console applicaiton and created files to cleanly and easily create such an application.  I also added an example application to show how to use it.

ECLIPSE PROJECT SETUP

You will need to add the following global build variables to your Eclipse workspace (menu Window->Preferences->C/C++->Build->Build Variables): 

BOOST_LOC : set to top level of boost instalation such as "/usr/local/boost_1_52_0"
CONSOLE_LIBRARY_LOC : set to top level of the console library repository such as "/ThirdParyTools/ConsoleLibrary"


